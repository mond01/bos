#include "mpi.h"
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <iostream>

void intArray_to_str(char* str, int* array, int size_array) {
    for (int i = 0; i < size_array; ++i) {
        sprintf(str + strlen(str), "%d ", array[i]);
    }
}

int main(int argc, char* argv[])
{
    int n;
    int rank;
    int opt;

    int *sendbuf;
    int *recvbuf;

    const int count_size = 3;
    int count[count_size] = {1,1,1};
    char buffer[1024] = "";
    char recvbuffer[1024] = "";

    int error = MPI_SUCCESS;
    char error_string[MPI_MAX_ERROR_STRING];

    MPI_Init(&argc, &argv);

    error = MPI_Comm_size(MPI_COMM_WORLD, &n);
    if (error != MPI_SUCCESS) {
        MPI_Error_string(error, error_string, NULL);
        fprintf(stderr, "%s \n", error_string);
        return error;
    }

    error = MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    if (error != MPI_SUCCESS) {
        MPI_Error_string(error, error_string, NULL);
        fprintf(stderr, "%s \n", error_string);
        return error;
    }

    int size_array = n;
    sendbuf = new int[size_array];

    int size_recvbuf = size_array / count_size;

    for (int i = 0; i < size_array; ++i)
        sendbuf[i]= rand() % 10 + 1;

    intArray_to_str(buffer, sendbuf, size_array);
    fprintf(stdout, "Ранг: %d, Sendbuf=[%s]\n", rank, buffer);


    recvbuf = new int[size_recvbuf];

    error = MPI_Reduce_scatter(sendbuf, recvbuf, count, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
    if (error != MPI_SUCCESS) {
        MPI_Error_string(error, error_string, NULL);
        fprintf(stderr, "%s \n", error_string);
        return error;
    }

    intArray_to_str(recvbuffer, recvbuf, size_array / count_size);
    fprintf(stdout, "Ранг: %d, Recvbuf=[%s]\n", rank, recvbuffer);

    delete[] recvbuf;
    delete[] sendbuf;

    MPI_Finalize();

    return error;
}
