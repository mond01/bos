#include <omp.h>
#include <stdio.h>

int main(int argc, char* argv[])
{
    int num_threads = atoi(argv[1]);
    int size = atoi(argv[2]);

    double starttime, endtime;

    int* v1 = (int*)malloc(sizeof(int)*size);
    int* v2 = (int*)malloc(sizeof(int)*size);
    int* v_res = (int*)malloc(sizeof(int)*size);

    for (int i = 0; i < size; ++i){
        v1[i] = i*i;
        v2[i] = i - 1;
    }

    starttime = omp_get_wtime();
    for (int i = 0; i < size; ++i)
        v_res[i] = v1[i] + v2[i];
    endtime = omp_get_wtime();
    printf("Время последовательной: %f сек.\n", endtime - starttime);

    omp_set_num_threads(num_threads);

    int i;

    starttime = omp_get_wtime();

    #pragma omp parallel for private(i) num_threads(num_threads)
    for (i = 0; i < size; ++i)
        v_res[i] = v1[i] + v2[i];
    endtime = omp_get_wtime();

    printf("Время параллельной: %f сек.\n", endtime - starttime);

    return 0;
}
