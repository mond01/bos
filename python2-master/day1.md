Задание на день 1
0. Проверить аккаунты на гитворк.
1. Запустить ВМ, нюансы [тут](proxy.md)
1. Документация фастапи https://fastapi.tiangolo.com/ . Fastapi и uvicorn надо ставить из pip, про прокси ниже. Задание: по документации запуститься и реализовать 3 ручки в fastapi (корень, получить item, создать item), показать консоль разработчика в chrome и умение работать с ней (request, responce, код возврата). Разобрать path, query и body params
1. Оффлайн версия фастапи, чтобы дока работала https://pypi.org/project/fastapi-offline/ . Показать post/put в автодокументации вашего приложения
1. Если хотите вывести многострочный вывод в endpoint, то вот костыль https://stackoverflow.com/a/71365862/19204439 . Позже будем рассматривать шаблонизатор и правильную работу с HTML
1. Разбираемся с postman. Показать коллекцию с 3 запросами в postman и работу с ним
1. Вспомнить проект тудушки, запустить его, расставить типы в тудушке.
1. Интерактивно: F12 - console

document.designMode = 'on';

ДЗ день 1:
1. Про аннотации https://habr.com/ru/company/lamoda/blog/432656/
2. Про датаклассы https://habr.com/ru/post/415829/
3. Разница моделей pydantic и датаклассов https://stackoverflow.com/questions/62011741/pydantic-dataclass-vs-basemodel
4. смотрим API в постмане https://youtu.be/cWW-dRWedXM

