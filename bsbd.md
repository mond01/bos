```sql
-- Создание таблицы "Регион"
CREATE TABLE region(
region_id SERIAL PRIMARY KEY,
region_name TEXT NOT NULL,
population INT NOT NULL
);

--Создание таблицы "Филиал"
CREATE TABLE branch(
branch_id SERIAL PRIMARY KEY,
branch_address VARCHAR(100),
branch_phone CHAR(12),
branch_email VARCHAR(50),
region_id INT NOT NULL,
FOREIGN KEY(region_id) REFERENCES region(region_id) ON DELETE CASCADE 
);

-- Создание таблицы "Дополнительные услуги"
CREATE TABLE extra_service(
extra_service_id SERIAL PRIMARY KEY,
name VARCHAR(50),
price REAL
);

-- Создание таблицы "Технология"
CREATE TABLE technology(
technology_id SERIAL PRIMARY KEY,
technology_name VARCHAR(50),
technology_max_speed REAL NOT NULL,
technology_wireless VARCHAR(100) NOT NULL
);

-- Создание таблицы "Тип услуги"
CREATE TABLE service_type(
service_type_id SERIAL PRIMARY KEY,
service_type_name VARCHAR(50) NOT NULL
);

-- Создание таблицы "Тариф"
CREATE TABLE tariff(
tariff_id SERIAL PRIMARY KEY,
tariff_name VARCHAR(50) UNIQUE,
tariff_max_speed REAL NOT NULL,
tariff_payment REAL NOT NULL,
tariff_price_per_GB REAL,
tariff_price_per_hour REAL,
service_type_id INT,
technology_id INT,
tariff_available BOOLEAN NOT NULL,
unlimitted BOOLEAN NOT NULL,
FOREIGN KEY(service_type_id) REFERENCES service_type(service_type_id) ON DELETE CASCADE,
FOREIGN KEY(technology_id) REFERENCES technology(technology_id) ON DELETE SET NULL 
);

-- Создание таблицы "Оказание услуг"
CREATE TABLE provision_of_services(
provision_service_id SERIAL PRIMARY KEY,
tariff_id INT NOT NULL,
connection_date DATE NOT NULL,
extra_service_id INT,
FOREIGN KEY(extra_service_id) REFERENCES extra_service(extra_service_id) ON DELETE SET NULL,
FOREIGN KEY(tariff_id) REFERENCES tariff(tariff_id) ON DELETE CASCADE
);

-- Создание таблицы "Пользователь"
CREATE TABLE users(
user_id SERIAL PRIMARY KEY,
user_name TEXT NOT NULL,
user_phone CHAR(12) NOT NULL UNIQUE,
user_email VARCHAR(50) NOT NULL UNIQUE,
user_address TEXT NOT NULL,
user_passport_info CHAR(11) NOT NULL UNIQUE,
branch_id INT NOT NULL,
provision_service_id INT NOT NULL,
FOREIGN KEY(branch_id) REFERENCES branch(branch_id)ON DELETE CASCADE,
FOREIGN KEY(provision_service_id) REFERENCES provision_of_services(provision_service_id) ON DELETE CASCADE
);

-- Заполнение таблицы "Дополнительные услуги"
INSERT INTO extra_service(name, price)
VALUES ('Премиум', 480),
       ('Дополнительный пакет 5 Гб', 100),
       ('Дополнительный пакет 10 Гб', 180),
       ('Дополнительный пакет 15 Гб', 250),
       ('Дополнительные 500 мин', 100),
       ('Дополнительные 1000 мин', 180),
       ('Дополнительно 300 сообщений', 150),
       ('Дополнительно 700 сообщений', 300);

-- Заполнение таблицы "Филиал"
INSERT INTO branch(branch_address, branch_phone, branch_email, region_id)
VALUES ('г. Москва, ул. Мичуринский проспект, д. 26', '+78001000800', 'kirovnetmoskva@mail.ru', 1),
       ('г. Санкт-Петербург, ул. Синопская набережная, д. 14', '+78003013011', 'kirovnetspb@mail.ru', 2),
       ('г. Нижний Новгород, ул. Большая Покровская, д. 56', '+78004015012', 'kirovnetnnovgorod@mail.ru', 3),
       ('г. Чебоксары, ул. Юрия Гагарина, д. 20А', '+78003006622', 'kirovnet21@mail.ru', 4),
       ('г. Краснодар, ул. Карасунская, д. 74', '+78003014411', 'kirovnetkrasnodar@mail.ru', 5);

-- Заполнение таблицы "Регион"
INSERT INTO region(region_name, population)
VALUES ('Московская область', 15000000),
       ('Ленинградская область', 10000000),
       ('Нижегородская область', 7000000),
       ('Чувашская республика', 1000000),
       ('Краснодарский край', 4000000);

-- Заполнение таблицы "Технология"
INSERT INTO technology(technology_name, technology_max_speed, technology_wireless)
VALUES ('Проводная региональная сеть', 10, 'Витая пара'),
       ('Проводная региональная сеть', 10000, 'Оптоволокно'),
       ('Проводная региональная сеть', 0.009765, 'Коаксиальный кабель'),
       ('Беспроводная региональная сеть', 0.097656, 'IEEE 802.11n'),
       ('Беспроводная региональная сеть', 0.048828, 'IEEE 802.11g');

-- Заполнение таблицы "Тип услуги"
INSERT INTO service_type(service_type_name)
VALUES ('Домашний интернет'), 
       ('Мобильный интернет'),
       ('Модем');

-- Заполнение таблицы "Тариф"
INSERT INTO tariff(tariff_name, tariff_max_speed, tariff_payment, tariff_price_per_GB, tariff_price_per_hour, service_type_id, technology_id, tariff_available, unlimitted)
VALUES 
    ('Минимум', 0.03418, 350, 25, 50, 2, 4, true, false),
    ('Минимум+', 0.03418, 420, 25, 50, 2, 4, false, false),
    ('Тарифище', 0.03418, 750, 25, 80, 2, 4, true, false),
    ('Smart', 0.03418, 650, 25, 65, 2, 4, true, false),
    ('Безлимитище', 0.03418, 550, 0, 0, 2, 4, false, true),
    ('Премиум', 0.04418, 1200, 25, 100, 2, 4, true, false),
    ('Технологии выгоды', 10, 1200, 0, 0, 1, 1, true, true),
    ('Апгрейд 500', 10, 500, 0, 0, 1, 1, true, true),
    ('2в1 Апгрейд 3.0', 10, 650, 0, 0, 1, 1, true, true),
    ('Всё ради науки', 10000, 15000, 0, 0, 1, 2, true, true),
    ('Закачайся!', 0.024414, 900, 25, 100, 3, 5, true, false);

-- Заполнение таблицы "Оказание услуг"
INSERT INTO provision_of_services(tariff_id, connection_date, extra_service_id) 
VALUES
    (6, '26.01.2002', 1),
    (6, '14.01.2021', 4),
    (4, '13.05.2020', 2),
    (9, '14.01.2021', NULL),
    (8, '13.05.2020', NULL),
    (3, '20.09.2010', 6),
    (10, '15.07.2012', NULL),
    (3, '20.09.2010', 6),
    (10, '15.07.2012', NULL),
    (5, '23.12.2019', NULL),
    (5, '14.05.2019', NULL),
    (5, '01.08.2018', NULL),
    (1, '05.04.2021', 3),
    (2, '03.11.2020', 1),
    (7, '04.10.2019', NULL),
    (11, '02.02.2020', 4),
    (8, '12.05.2022', NULL),
    (3, '25.09.2017', 1),
    (1, '15.10.2016', NULL),
    (4, '20.10.2020', 5),
    (4, '14.08.2020', 2),
    (9, '01.06.2018', NULL),
    (9, '07.08.2020', NULL),
    (4, '03.10.2019', 1),
    (4, '10.10.2018', 3),
    (5, '05.07.2016', NULL),
    (8, '13.09.2021', NULL),
    (7, '25.04.2017', NULL),
    (3, '15.01.2018', 1),
    (2, '20.10.2018', 6),
    (1, '15.04.2021', 2),
    (9, '30.09.2019', NULL),
    (9, '12.08.2021', NULL),
    (11, '03.04.2020', 4),
    (4, '10.02.2020', 1),
    (9, '01.12.2016', NULL),
    (4, '13.10.2020', 8);

-- Заполнение таблицы "Пользователь"
INSERT INTO users(user_name, user_phone, user_email, user_address, user_passport_info, branch_id, provision_service_id) VALUES
    ('Иванов Д.В.', '+79153531290', 'ivanov@yandex.ru', 'г.Москва, ул.Ломоносовский пр-т, д.4, кв.14', '4616 129340', 1, 1),
    ('Петров И.А.', '+79163451291', 'petrov@mail.ru', 'г.Москва, ул.Зелёная, д.32, кв.54', '4615 193123', 1, 2), 
    ('Сидоров К.К.', '+79351905416', 'sidorov@mail.ru', 'г.Москва, ул.Танковая, д.20, кв.45', '4615 167540', 1, 3),
    ('Потёмкин Г.А', '+79441890534', 'potemkin@yandex.ru', 'г.Краснодар, ул.Таврическая, д.15, кв.105', '3415 535890', 5, 4),
    ('Румянцев П.А.', '+79449807854', 'rumyancev@yandex.ru', 'г.Краснодар, ул.Кагульская, д.45, кв.151', '3412 586123', 1, 5),
    ('Спиридов Г.А', '+79439867414', 'spiridov@yandex.ru', 'г.Краснодар, ул.Чесменская, д.38, кв.201', '3410 485190', 5, 6),
    ('Ушаков Ф.Ф.', '+79672341208', 'ushakov@yandex.ru', 'г.Нижний Новгород, ул.Керченская, д.8, кв.25', '2516 124956', 3, 7), 
    ('Орлов Г.Г.', '+79676573405', 'orlovg@yandex.ru', 'г.Нижний Новгород, ул.Чесменская, д.15, кв.98', '2515 163825', 3, 8),
    ('Орлов Г.А.', '+79676573450', 'orlova@yandex.ru', 'г.Нижний Новгород, ул.Чесменская, д.15, кв.98', '2515 502583', 3, 9),
    ('Панин Н.И.', '+79890985612', 'panin@mail.ru', 'г.Санкт-Петербург, ул.Екатеринская, д.15, кв.101', '4312 195614', 2, 10),
    ('Безбородко А.А', '+79891324118', 'bezborodko@yandex.ru', 'г.Санкт-Петербург, ул.Османская, д.42, кв.124', '4310 109357', 2, 11),
    ('Кутузов М.И.', '+79140951225', 'kutuza@mail.ru', 'г.Москва, ул.Бородинская, д.12, кв.35', '4513 106126', 1, 12),
    ('Фальконе Э.М', '+79450944315', 'falcone@gmail.ru', 'г.Санкт-Петербург, ул.Сенатская площадь, д.45, кв.98', '3867 098654', 2, 13),
    ('Меньшиков Д.А', '+79130871415', 'menshikov@yandex.ru', 'г.Москва, ул.Мичуринский проспект, д.4, кв.15', '4513 098769', 1, 14),
    ('Шувалов И.И', '+78574501245', 'shuvalov@yandex.ru', 'г.Москва, ул.Академическая, д.1, 67', '4214 593125', 1, 15),
    ('Пугачёв Е.И.', '+79020981321', 'pugachev@gmail.ru', 'г.Нижний Новгород, ул.Донская, д.3, кв.37', '2615 297457', 3, 16),
    ('Багратион П.И.', '+79361944316', 'bagration@yandex.ru', 'г.Краснодар, ул.Егерская, д.86, кв.176', '2813 356795', 5, 17),
    ('Воронцов М.С.', '+79245190581', 'voroncov@yandex.ru', 'г.Чебоксары, ул.Юго-Западная, д.14, кв.176', '3419 125846', 4, 18),
    ('Бецкой И.И.', '+78473526345', 'beckoy@mail.ru', 'г.Цивильск, ул.Пионерская, д.25, 14', '2578 836276', 4, 19),
    ('Дашкова Е.Р.', '+79121377843', 'dashkova@yandex.ru', 'г.Москва, ул.Ломоносовский пр-т, д.12, кв.98', '4616 694309', 1, 20),
    ('Воронцова Е.Р.', '+79451239054', 'voroncova@mail.ru', 'г.Нижний Новгород, ул.Зелёная, д.32, кв.154', '4615 679205', 3, 21),
    ('Салтыкова Д.П.', '+79526831048', 'saltykova@yandex.ru', 'г.Краснодар, ул.Танковая, д.20, кв.45', '4115 952951', 5, 22),
    ('Голицына Н.П.', '+79121843534', 'golycina@mail.ru', 'г.Краснодар, ул.Таврическая, д.65, кв.105', '3814 793025', 5, 23),
    ('Чичагов В.Я.', '+79446920234', 'chichagov@yandex.ru', 'г.Санкт-Петербург, ул.Адмиралтейская, д.25, кв.135', '3915 515892', 2, 24),
    ('Грейг С.К.', '+79329207654', 'greig@gmail.ru', 'г.Москва, ул.Гогландская, д.34, кв.221', '4512 485190', 5, 6),
    ('Нахимов П.С.', '+79272143288', 'nahimov@yandex.ru', 'г.Москва, ул.Керченская, д.14, кв.35', '2536 184956', 1, 26),
    ('Суворова В.И.', '+79221367843', 'suvorova@yandex.ru', 'г.Москва, ул.Измаиловская, д.26, кв.129', '4616 602743', 1, 27),
    ('Орлова Е.Н.', '+79251163804', 'orlova@mail.ru', 'г.Санкт-Петербург, ул.Каховская, д.125, кв.24', '4315 649225', 2, 28),
    ('Браницкая А.В.', '+79316581245', 'branickaya@mail.ru', 'г.Чебоксары, ул.Тракторостроителей, д.48, кв.167', '4315 716582', 4, 29),
    ('Разумовская Е.И.', '+79421143498', 'razumovskaya@mail.ru', 'г.Нижний Новгород, ул.Запорожская, д.53, кв.135', '4114 601753', 3, 30),
    ('Столыпин П.А.', '+79296620234', 'stolypin@yandex.ru', 'г.Москва, ул.Площадь Революции, д.1, кв.135', '4545 710957', 1, 31),
    ('Витте С.Ю', '+79336230761', 'vitte@mail.ru', 'г.Москва, ул.Гогландская, д.5, кв.121', '4512 196634', 1, 32),
    ('Сперанский М.М.', '+79564936495', 'speranskiy@yandex.ru', 'г.Краснодар, ул.Керченская, д.13, кв.25', '2536 791658', 5, 33),
    ('Грибоедов А.С.', '+79241867833', 'griboedov@mail.ru', 'г.Чебоксары, ул.Измаиловская, д.25, кв.195', '6917 105678', 4, 34),
    ('Аракчеев А.А.', '+79151563401', 'arakcheev@yandex.ru', 'г.Москва, ул.Каховская, д.65, кв.124', '1679 503759', 1, 35);

-- Задачи легкого уровня (1 балл)

-- Вывести названия всех ДОСТУПНЫХ тарифов, а также их цену
SELECT tariff_name AS "Название тарифа", tariff_payment AS Цена
FROM tariff
WHERE tariff_available = true;

-- Вывести среднюю цену безлимитных тарифов
SELECT AVG(tariff_payment) AS "Средняя цена"
FROM tariff
WHERE unlimitted = true;

-- Вывести суммарную цену за все дополнительные услуги
SELECT SUM(price) AS "Общая цена"
FROM extra_service;

-- Вывести самую высокую скорость интернета 
SELECT MAX(tariff_max_speed) AS Скорость
FROM tariff;

-- Вывести название тарифа и его скорость в порядке убывания скорости (вывести только первые 5 результатов)
SELECT tariff_name AS "Название тарифа", tariff_max_speed AS Скорость
FROM tariff
ORDER BY tariff_max_speed DESC LIMIT 5;

/* В филиале Чувашской республики новый пользователь, вот его данные: 
	- ФИО: Колчак А.В.
	- тел.: +79526492465
	- email: kolchak@yandex.ru
	- адрес: г. Чебоксары, ул. Тракторостроителей, д.14, кв. 34
	- паспорт: 3857 105723
	- регион: Чувашская республика
	- id оказ. услуги: 36
   А вот предосавляемые ему услуги:
	- тариф: Безлимитище
	- дата подключения: 31.10.2022
	- доп. услуга: доп. пакет 15 гб
   Заполните таблицу новыми данными 
*/

INSERT INTO provision_of_services(tariff_id, connection_date, extra_service_id)
VALUES (6, '31.10.2022', 4);
INSERT INTO users(user_name, user_phone, user_email, user_address, user_passport_info, region_id, provision_service_id)
VALUES ('Колчак А.В.', '+79526492465', 'kolchak@yandex.ru', 'г. Чебоксары, ул. Тракторостроителей, д.14, кв. 34', '3857 105723', 4, 36);

-- В отношении users кое-что поменялось: пользователь Салтыкова Д.П. поменяла свой номер на +79150914567. Напишите запрос для изменения данных
UPDATE users 
SET user_phone = '+79150914567' WHERE user_name = 'Салтыкова Д.П.';

-- Вывести информацию о всех пользователях, отчество и имя которых начинается на "А"
SELECT * FROM users
WHERE user_name LIKE '%А.А.';

-- Задачки среднего уровня сложности (2 балла)

-- Вывести минимальную, максимальную и среднюю цену тарифа для каждого вида технологии (действовать без JOIN'ов, только с technology_id). Среднюю цену округлить до двух знаков после запятой
SELECT technology_id, MIN(tariff_payment) AS "Минимальная цена", MAX(tariff_payment) AS "Максимальная цена", AVG(tariff_payment) AS "Средняя цена"
FROM tariff
GROUP BY technology_id;

-- Вывести максимальную и минимальную цену тарифа для каждой технологии, кроме "Оптоволокна" (аналогично без JOIN'ов) 
SELECT technology_id, MAX(tariff_payment) AS "Максимальная цена", MIN(tariff_payment) AS "Минимальная цена" 
FROM tariff
GROUP BY technology_id
HAVING technology_id <> 2;

-- Вывести информацию о самых дешёвых тарифах в отношении tariff
SELECT tariff_name, tariff_payment AS Цена
FROM tariff
WHERE tariff_payment = (SELECT MIN(tariff_payment)
                        FROM tariff);

-- Вывести информацию о тарифах, цена которых отличается от средней цены на 200 руб. Из выборки средней цены исключить технологию оптоволокна.
SELECT tariff_name, tariff_payment AS Цена
FROM tariff
WHERE ABS(tariff_payment - (SELECT AVG(tariff_payment) 
                            FROM tariff
                            WHERE technology_id <> 2)) > 200;

-- Вывести информацию о тех технологиях в отношении tariff, у которых количество тарифов больше 2.
SELECT DISTINCT technology_id 
FROM tariff
WHERE technology_id IN (
            SELECT technology_id
            FROM tariff
            GROUP BY technology_id
            HAVING COUNT(tariff_name) > 2 
);

-- Задачки повышенного уровня сложности (3 балла)

-- Вывести регион, в котором больше всего пользователей провайдера
SELECT region_name, COUNT(region_name) AS Количество 
FROM
    region INNER JOIN users on region.region_id = users.region_id
GROUP BY region_name
ORDER BY Количество DESC LIMIT 1;

-- Вывести для каждого региона адрес филиала
SELECT region_name, branch_address
FROM 
    region INNER JOIN branch ON region.branch_id = branch.branch_id;

-- Вывести имя пользователя, название тарифа, название доп. услуги и цену за всё это для пользователей мобильного интернета в порядке понижения цены
SELECT users.user_name, tariff.tariff_name AS Тариф, 
       extra_service.name AS "Доп. услуга", 
       (tariff.tariff_payment + extra_service.price) AS Цена
FROM tariff 
        INNER JOIN provision_of_services ON tariff.tariff_id = provision_of_services.tariff_id
        INNER JOIN extra_service ON provision_of_services.extra_service_id = extra_service.extra_service_id
        INNER JOIN users ON provision_of_services.provision_service_id = users.provision_service_id
GROUP BY users.user_name, tariff.tariff_name, extra_service.name, tariff.tariff_payment, extra_service.price
ORDER BY Цена DESC;

-- Вывести самый популярный тариф мобильного интернета и количесто пользователей
SELECT tariff.tariff_name, COUNT(provision_of_services.tariff_id) AS Количество
FROM
    tariff INNER JOIN provision_of_services ON tariff.tariff_id = provision_of_services.tariff_id
GROUP BY tariff.tariff_name
ORDER BY Количество DESC LIMIT 1;

-- Увеличить цену на 10% для самого популярного тарифа мобильного интернета
UPDATE tariff
SET tariff_payment = tariff_payment + (tariff_payment * 0.1)
WHERE
tariff_name = (SELECT tariff.tariff_name 
		FROM tariff
		INNER JOIN provision_of_services ON tariff.tariff_id = provision_of_services.tariff_id
		GROUP BY tariff.tariff_name
		ORDER BY COUNT(provision_of_services.tariff_id) DESC LIMIT 1);

-- Задания со звёздочкой (6 баллов)

-- Вывести информацию о размере всех баз данных
SELECT pg_database.datname,
       pg_size_pretty(pg_database_size(pg_database.datname)) AS size
FROM pg_database
ORDER BY pg_database_size(pg_database.datname) DESC;

-- Вывести детальную информацию о каждой таблице с указанием её схемы, размера без индексов, размере индексов, суммарном размере таблицы и индексов, а также количестве строк в таблице
SELECT schemaname,
       C.relname AS "relation",
       pg_size_pretty (pg_relation_size(C.oid)) as table,
       pg_size_pretty (pg_total_relation_size (C.oid)-pg_relation_size(C.oid)) as index,
       pg_size_pretty (pg_total_relation_size (C.oid)) as table_index,
       n_live_tup
FROM pg_class C
LEFT JOIN pg_namespace N ON (N.oid = C .relnamespace)
LEFT JOIN pg_stat_user_tables A ON C.relname = A.relname
WHERE nspname NOT IN ('pg_catalog', 'information_schema')
AND C.relkind <> 'i'
AND nspname !~ '^pg_toast'
ORDER BY pg_total_relation_size (C.oid) DESC
```

