Начинаете работу со связного клона ВМ с диска D:/Courses/2802/Ubuntu 22.04 v4

ВМ есть на всех компах. Проверяйте доступную оперативу, если на хосте всего 8 гигов, то в настройках ВМ ставьте 5гб. Дальше делаем связный клон.

Флешки можно пробрасывать сразу на ВМ. Для этого при включенной ВМ после присоединения флешки в предлагаемом меню выбираете – к виртуалке, на запущенной машине

Везде login надо заменить на ваш логин с хостовой ОС, а pass - на пароль от хостовой ОС

Перед работой надо проверить
0. доступность gitwork.ru. Если какие-то проблемы, то обновить chrome. Нужный deb-пакет лежит в корне сервера в public. Переносим его в виртуалку (работает drag-n-drop в наутилус, но не на рабочий стол), потом выполняем sudo dpkg -i <имя пакета с хромом>
1. ya.ru через chrome на хосте
2. ya.ru через chrome на ВМ - дальше только на ВМ
3. sudo apt update
4. sudo apt install pachi
5. pip install --user fastapi
6. sudo docker pull python:3.10-slim

7. Если что-то не работает, то чиним. Идём в настройки сети на ВМ, сетевой прокси

    прокси для HTTP и для HTTPS вписываем

    192.168.232.1

    порт 3128

    в исключения добавляем gitwork.ru

    после этого открываем ya.ru в браузере, вводим логин и пароль от хоста

8. для настройки apt

    sudo vim /etc/apt/apt.conf

    Вписываем в новый файл строку

    Acquire::http::Proxy "http://login:pass@192.168.232.1:3128";

9. pip install --user --proxy http://login:pass@192.168.232.1:3128 fastapi

    login и pass от хоста

10. по гайду https://docs.docker.com/config/daemon/systemd/ настраиваем прокси для докера (сам гайд не нужен, ниже все ключевые шаги даны)

    sudo mkdir -p /etc/systemd/system/docker.service.d

    в файл

    /etc/systemd/system/docker.service.d/http-proxy.conf

    пишем

    ```
    [Service]
    Environment="HTTP_PROXY=http://login:pass@192.168.232.1:3128"
    Environment="HTTPS_PROXY=http://login:pass@192.168.232.1:3128"
    ```

    перезагружаем докер-демон для применения настроек
    ```bash
    sudo systemctl daemon-reload
    sudo systemctl restart docker
    ```

    проверяем, что прокси проброшен

    sudo systemctl show --property=Environment docker

    Должно быть

    Environment=HTTP_PROXY...

    Если environment пуст, то проверьте правильность заполнения конфига

    теперь работает pull
