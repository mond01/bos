#include "mpi.h"
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <iostream>


int main(int argc, char* argv[])
{
    int number;
    int n;
    int rank;

    char *outbuf;
    int outsize;
    int position;

    double double_number;

    char buffer[1000] = "";
    char symbol;

    int error = MPI_SUCCESS;
    char error_string[MPI_MAX_ERROR_STRING];

    MPI_Init(&argc, &argv);

    error = MPI_Comm_size(MPI_COMM_WORLD, &n);
    if (error != MPI_SUCCESS) {
        MPI_Error_string(error, error_string, NULL);
        fprintf(stderr, "%s \n", error_string);
        return error;
    }

    error = MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    if (error != MPI_SUCCESS) {
        MPI_Error_string(error, error_string, NULL);
        fprintf(stderr, "%s \n", error_string);
        return error;
    }

   if(rank == 0)
   {
       number = 11;
       double_number = 17.03;
       symbol = 'p';

       fprintf(stdout, "Ранг: %d. Данные(упак): int=[%d], double=[%lf], char=[%c]\n", rank, number, double_number, symbol);

       outsize = sizeof(number)+ sizeof(double_number) + sizeof(symbol);
       outbuf = new char[outsize];
       position = 0;

       MPI_Pack(&number, 1, MPI_INT, outbuf, outsize, &position, MPI_COMM_WORLD);
       MPI_Pack(&double_number, 1, MPI_DOUBLE, outbuf, outsize, &position, MPI_COMM_WORLD);
       MPI_Pack(&symbol, 1, MPI_CHAR, outbuf, outsize, &position, MPI_COMM_WORLD);

   }

   //передаём всем массивам outsize, чтобы они смогли выделить память под outbuf
   error = MPI_Bcast(&outsize, 1, MPI_INT, 0, MPI_COMM_WORLD);
   if (error != MPI_SUCCESS) {
       MPI_Error_string(error, error_string, NULL);
       fprintf(stderr, "%s \n", error_string);
       return error;
   }

   if(rank != 0)
   {
       outbuf = new char[outsize];
   }

   //передаём упакованные данные
   error = MPI_Bcast(outbuf, outsize, MPI_CHAR, 0, MPI_COMM_WORLD);
   if (error != MPI_SUCCESS) {
       MPI_Error_string(error, error_string, NULL);
       fprintf(stderr, "%s \n", error_string);
       return error;
   }

   if(rank != 0)
   {
       position = 0;
       MPI_Unpack(outbuf, outsize, &position, &number, 1, MPI_INT, MPI_COMM_WORLD);
       MPI_Unpack(outbuf, outsize, &position, &double_number, 1, MPI_DOUBLE, MPI_COMM_WORLD);
       MPI_Unpack(outbuf, outsize, &position, &symbol, 1, MPI_CHAR, MPI_COMM_WORLD);

       fprintf(stdout, "Ранг: %d. Данные(расп): int=[%d], double=[%lf], char=[%c]\n", rank, number, double_number, symbol);
   }

   delete[] outbuf;

   MPI_Finalize();

   return error;
}
