#include <omp.h>
#include <stdio.h>

int main(int argc, char* argv[])
{
    int num_threads = atoi(argv[1]);
    int size = atoi(argv[2]);

    double starttime, endtime;
    int sum;

    int** m1 = (int*)malloc(sizeof(int*)*size);
    int** m2 = (int*)malloc(sizeof(int*)*size);
    int** m_res = (int*)malloc(sizeof(int*)*size);

    omp_set_num_threads(num_threads);

    for (int i = 0; i < size; ++i){
        m1[i] = (int*)malloc(sizeof(int)*size);
        m2[i] = (int*)malloc(sizeof(int)*size);
        m_res[i] = (int*)malloc(sizeof(int)*size);
        for (int l = 0; l < size; ++l){
            m1[i][l] = i*i + l;
            m2[i][l] = size + l*l;
        }
    }

    starttime = omp_get_wtime();
    int i, h;
    #pragma omp parallel for
        for (int i = 0; i < size; ++i)
            for (int l = 0; l < size; ++l)
                    for(int h = 0; h < size; ++h)
                        m_res[i][l] += m1[i][h] * m2[h][l];

    endtime = omp_get_wtime();

    printf("Время (поэл-е): %f сек.\n", endtime - starttime);

    starttime = omp_get_wtime();

    #pragma omp parallel for private(i, h)
            for (i = 0; i < size; ++i){
                int j = i / size;
                int k = i % size;
                for (h = 0; h < size; ++h)
                    m_res[i][j] += m1[i][h] * m2[h][j];
            }


    endtime = omp_get_wtime();

    printf("Время (постолб-ое): %f сек.\n", endtime-starttime);


    return 0;
}
