#include <omp.h>
#include <stdio.h>

int main(int argc, char* argv[])
{
    int num_threads = atoi(argv[1]);
    int size = atoi(argv[2]);

    double starttime, endtime;

    int** m1 = (int*)malloc(sizeof(int*)*size);
    int** m2 = (int*)malloc(sizeof(int*)*size);

    omp_set_num_threads(num_threads);

    int i, j;

    #pragma omp parallel for schedule(dynamic, 1) private(i,j)
    for (i = 0; i < size; ++i){
        m1[i] = (int*)malloc(sizeof(int)*size);
        m2[i] = (int*)malloc(sizeof(int)*size);
        #pragma omp parallel for schedule(dynamic, 1) private(j)
        for (j = 0; j < size; ++j){
            m1[i][j] = i*i +j;
        }
    }

    starttime = omp_get_wtime();
    #pragma omp parallel for schedule(dynamic, 1) private(i,j)
    for (i = 0; i < size; ++i)
        for (j = 0; j < size; ++j)
            m2[i][j] = m1[j][i];
    endtime = omp_get_wtime();

    printf("Время: %f сек.\n", endtime - starttime);

    return 0;

}
