**Лекционные и справочные материалы для выполнения ЛР:**

[lec/MPI1.pdf](https://gitwork.ru/sub/parallel/-/blob/master/lec/MPI1.pdf)

**Задание на ЛР:**

1. Разработать параллельную программу, использующую базовые конструкции MPI (получение числа процессоров, своего ранга и имени вычислительного модуля). Программа должна производить вывод информации в стандартный поток вывода, а сообщения об ошибках – в стандартный поток ошибок.
1. Разработать параллельную программу, осуществляющую разделение коммуникатора из лекционного примера. Программа должна вывести ранг в старом коммуникаторе, число процессоров и ранг в новом коммуникаторе. Если процесс не попал в новый коммуникатор, должен сообщить об этом. 
1. Смоделировать последовательный двусторонний обмен сообщениями между двумя процессами, замерить с помощью функции MPI_Wtime() время на одну итерацию обмена, определить зависимость времени от длины сообщения.
1. Сравнить время реализации пересылок данных между двумя выделенными процессорами с блокировкой и без блокировки.
1. Реализовать при помощи отправки сообщений типа "точка-точка" следующие схемы коммуникации процессов:
    1) Передача данных по кольцу, два варианта: “эстафетная палочка” (очередной процесс дожидается сообщения от предыдущего и потом посылает следующему) и “сдвиг”(одновременная посылка и прием сообщений) 
    2) master-slave (все процессы общаются с одним) 
    3) Пересылка данных от каждого процесса каждому
1. С использованием  неблокирующих операций осуществить транспонирование квадратной матрицы, распределенной между процессорами по строкам.
1. Составление письменного отчета по лабораторной работе.

Пример использования MPI_Wtime():
```
double starttime, endtime;
starttime = MPI_Wtime();
....  stuff to be timed  ...
endtime   = MPI_Wtime();
printf("That took %f seconds\n",endtime-starttime);
```

**Пример оформления отчета о ЛР:**

[Пример_отчета_о_ЛР_MS_Office.docx](https://gitwork.ru/sub/apvs/-/blob/master/%D0%9F%D1%80%D0%B8%D0%BC%D0%B5%D1%80_%D0%BE%D1%82%D1%87%D0%B5%D1%82%D0%B0_%D0%BE_%D0%9B%D0%A0_MS_Office.docx)

[Пример_отчета_о_ЛР_Libre_Office.odt](https://gitwork.ru/sub/apvs/-/blob/master/%D0%9F%D1%80%D0%B8%D0%BC%D0%B5%D1%80_%D0%BE%D1%82%D1%87%D0%B5%D1%82%D0%B0_%D0%BE_%D0%9B%D0%A0_Libre_Office.odt)
