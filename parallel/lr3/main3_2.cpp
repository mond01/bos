#include "mpi.h"
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <iostream>

#define TAG 11

void show_matrix(int *matrix, int size_matrix)
{
    fprintf(stdout, "\n");
    for (int i = 0; i < size_matrix * size_matrix; ++i)
    {
        if(i != 0 && i % size_matrix == 0)
        {
            fprintf(stdout, "\n");
        }
        fprintf(stdout, "%d ", matrix[i]);
    }
    fprintf(stdout, "\n");
}

void create_matrix(int *matrix, int size_matrix)
{
    for (int i = 0; i < size_matrix * size_matrix; ++i)
            matrix[i]= rand() % 10 +1;
}

int main(int argc, char* argv[])
{
    int n;
    int rank;
    int size_matrix;
    int opt;
	int size_of_int;

    int error = MPI_SUCCESS;
    char error_string[MPI_MAX_ERROR_STRING];
	
    MPI_Status status;

    MPI_Datatype row;
    MPI_Datatype column, xpose;

    MPI_Init(&argc, &argv);

    error = MPI_Comm_size(MPI_COMM_WORLD, &n);
    if (error != MPI_SUCCESS) {
        MPI_Error_string(error, error_string, NULL);
        fprintf(stderr, "%s \n", error_string);
        return error;
    }

    error = MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    if (error != MPI_SUCCESS) {
        MPI_Error_string(error, error_string, NULL);
        fprintf(stderr, "%s \n", error_string);
        return error;
    }

    while ((opt = getopt(argc, argv, "n:")) != -1) {
        if (optarg != 0) {
            sscanf(optarg, "%d", &size_matrix);
        }
    }

    int *matrix1;
    int *res_matrix;

    //вторая матрица необходима всем процессам для расчетов
    int *matrix2 = new int [size_matrix * size_matrix];

    int piece_of_data = size_matrix / n;

    //каждому процессу память для принятия кусочков 1-й матрицы
    //и память результата перемножения этих кусочков на 2-ю матрицу
    int *piece1 = new int [size_matrix * piece_of_data];
    int *piece3 = new int [size_matrix * piece_of_data];

    for(int i = 0; i < size_matrix * piece_of_data; ++i)
    {
        piece3[i] = 0;
    }

    //процесс 0 генерирует матрицу и рассылает всем кусочки данных для расчета
    if(rank == 0)
    {
        matrix1 = new int [size_matrix * size_matrix];
        res_matrix = new int [size_matrix * size_matrix];

        create_matrix(matrix1, size_matrix);
        create_matrix(matrix2, size_matrix);

        show_matrix(matrix1, size_matrix);
        show_matrix(matrix2, size_matrix);

    }

    //свой тип данных - строка матрицы
    MPI_Type_contiguous(size_matrix, MPI_INT, &row);
    MPI_Type_vector(size_matrix,1,size_matrix,MPI_INT,&column);//тип столбец
    MPI_Type_hvector(size_matrix, 1, 4, column, &xpose);//тип матрица

    //закрепим тип перед информационными обменами
    MPI_Type_commit(&row);
    MPI_Type_commit(&column);
    MPI_Type_commit(&xpose);

    //процесс 0 всем отправляет matrix2
    if(rank == 0)
    {
	for(int i = 1; i < n; ++i)
	{
	    MPI_Send((void*)matrix2, 1, xpose, i, TAG, MPI_COMM_WORLD);
	}
    } else {
	MPI_Recv((void*)matrix2, size_matrix*size_matrix, MPI_INT, 0, TAG, MPI_COMM_WORLD, &status);
    }

    //процесс 0 отправляет всем проц кусочки из первой матрицы для подсчета
    MPI_Scatter(matrix1, piece_of_data, row, piece1, piece_of_data, row, 0, MPI_COMM_WORLD);

    //каждый проц будет перемножать свои кусочки из первой матрицы на вторую
    for (int i = 0; i < size_matrix * piece_of_data; ++i) {
        for (int j = 0; j < size_matrix; ++j) {
            piece3[i] += piece1[size_matrix * (i / size_matrix) + j] * matrix2[size_matrix * j + i % size_matrix];//деление будет целочисленное, так как целое делится на целое
        }
    }

    //отправляем обратно процессу 0 свои рассчёты
    MPI_Gather(piece3, piece_of_data, row, res_matrix, piece_of_data, row, 0, MPI_COMM_WORLD);

    MPI_Type_free(&row);
    MPI_Type_free(&column);
    MPI_Type_free(&xpose);

    if(rank == 0)
    {
        show_matrix(res_matrix, size_matrix);
    }

    MPI_Finalize();

    return error;
}
